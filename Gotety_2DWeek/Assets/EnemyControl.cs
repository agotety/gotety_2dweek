﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{

    private GameObject player;

    public float moveSpeed;

    public float playerRange;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
    }
}
