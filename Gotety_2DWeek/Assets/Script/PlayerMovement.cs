﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public GameObject projectilePrefab;
    //public Transform shotSpawn;
    public float shotSpeed;

    public TextMeshProUGUI countText;
    public float runSpeed;
    public float jumpForce;

    private int count;

    public SpriteRenderer spriteRenderer;

    public Animator animator;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            //print("jump");

            int levelMask = LayerMask.GetMask("Level");
            //print(levelMask + "lm");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            { Jump(); }

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                shoot(); 
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Gem"))
        {
            other.gameObject.SetActive(false);

            //Debug.Log("yo!");

            count = count + 1;

            SetCountText();
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = true;

        if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = false;

        if (Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("IsRunning", true);
            //print("IsRunning");
        }
        else
        {
            //print("not running");
            animator.SetBool("IsRunning", false);
        }
    }

    void Jump()
    {
        // print("jumpman");
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);

    }


    void shoot()
    {

        print("shoot");
        GameObject projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
        Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
        projectileRB.velocity = transform.up * shotSpeed;
    }
    void SetCountText()
    {
        countText.text = "Gem Count: " + count.ToString();
    }
}